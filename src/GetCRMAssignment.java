import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class GetCRMAssignment {
    private final static Double DAY_TIME_AMOUNT = 16.0;
    private static Map<Integer, Double> importanceTimeCostMap =
            new HashMap<>() {{
        put(1, 6.0);        put(2, 4.0);        put(3, 4.0);        put(4, 3.5);
        put(5, 1.5);        put(6, 5.5);        put(7, 10.0);        put(8, 5.0);
        put(9, 2.0);        put(10, 5.0);        put(11, 8.0);        put(12, 7.0);
        put(13, 2.0);        put(14, 1.0);        put(15, 9.0);        put(16, 7.0);
        put(17, 1.0);        put(18, 3.0);        put(19, 2.0);        put(20, 12.0);
    }};
    private static List<Integer> route;

    public static void main(String[] args) {
        buildRouteInOrderOfPriority();
        buildRouteInOrderOfPriorityAndTimeCost();
    }
    private static void buildRouteInOrderOfPriority() {
        System.out.println("\nMethod : higher priority is better, timecost doesnt matter");
        route = new ArrayList<>();
        Double currentSpentTime = null;
        ConcurrentHashMap<Integer, Double> currentMethodMap = new ConcurrentHashMap<>();
        for (int i = 1; i <= importanceTimeCostMap.size(); i++) {
            currentMethodMap.put(i, importanceTimeCostMap.get(i));
        }
        for (int day = 1; day <= 2; day++) {
            currentSpentTime = 0.0;
            for(Map.Entry<Integer, Double> element : currentMethodMap.entrySet()) {
                if (currentSpentTime < DAY_TIME_AMOUNT && (DAY_TIME_AMOUNT - currentSpentTime >= element.getValue())) {
                    route.add(element.getKey());
                    currentSpentTime += element.getValue();
                    currentMethodMap.remove(element.getKey());
                }
            }
        }
        System.out.println("\nOptimal Route:   " + route);
        System.out.println("\n=======================================");
    }

    private static void buildRouteInOrderOfPriorityAndTimeCost() {
        System.out.println("\nMethod : higher priority AND lower timecost is better\n");
        Integer highestPriority = null;
        Double lowestTime = null;
        route = new ArrayList<>();
        Double currentSpentTime = null;
        TreeMap<Integer, Double> treeMap = new TreeMap<>(Collections.reverseOrder());
        for (int i = 1; i <= importanceTimeCostMap.size(); i++) {
            treeMap.put(i, importanceTimeCostMap.get(i));
        }
        for (int day = 1; day <= 2; day++) {
            currentSpentTime = 0.0;
            while (currentSpentTime < 16.0) {
                highestPriority = 999;
                lowestTime = 999.0;
                for (Map.Entry<Integer, Double> element : treeMap.entrySet()) {
                    if (element.getKey() < highestPriority && element.getValue() <= lowestTime) {
                        highestPriority = element.getKey();
                        lowestTime = element.getValue();
                    }
                }
                if ((16.0 - currentSpentTime - treeMap.get(highestPriority)) > 0) {
                    System.out.println("Going to location with priority: " + highestPriority + "; Estimated time to spent, hours: " + treeMap.get(highestPriority));
                    currentSpentTime += treeMap.get(highestPriority);
                    System.out.println("Current day " + day + " time remaining after visit, hours: " + (16.0 - currentSpentTime));
                    treeMap.remove(highestPriority);
                    route.add(highestPriority);
                    System.out.println();
                } else if ((16.0 - currentSpentTime - treeMap.get(highestPriority)) == 0) {
                    System.out.println("Going to location with priority: " + highestPriority + "; Estimated time to spent, hours: " + treeMap.get(highestPriority));
                    currentSpentTime += treeMap.get(highestPriority);
                    System.out.println("Current day " + day + " time remaining after visit, hours: " + (16.0 - currentSpentTime));
                    treeMap.remove(highestPriority);
                    route.add(highestPriority);
                    System.out.println("!Dont have time to visit any more locations today");
                    System.out.println("!Day " + day + " ends!");
                    System.out.println();
                }
                else {
                    currentSpentTime += treeMap.get(highestPriority);
                    System.out.println("!Dont have time to visit any more locations today");
                    System.out.println("!Day " + day + " ends!");
                    System.out.println();
                }
            }
        }
        System.out.println("Optimal Route:  " + route);
    }
}
